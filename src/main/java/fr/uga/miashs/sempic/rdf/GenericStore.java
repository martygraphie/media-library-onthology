/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.uga.miashs.sempic.rdf;

import fr.uga.miashs.sempic.model.rdf.SempicOnto;
import java.util.*;
import javax.ejb.Stateless;
import org.apache.jena.rdf.model.*;

/**
 *
 * @author Jerome David <jerome.david@univ-grenoble-alpes.fr>
 */
@Stateless
public class GenericStore extends RDFStore{    
    
    /**
     * Retrieve the instances that contain the string labelContent in one of their labels
     * @param type the type of instances
     * @param labelContent the content of the instance labels
     * @param instanciatedProperty if not null the instance has to be object of this given property
     * @param ownerId if not -1 the subject (photo) of the instanciatedProperty has to be owned by the given user
     * @return 
     */
    public List<Resource> lookupInstances(Resource type, String labelContent, Resource instanciatedProperty, long ownerId) {
        String query = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> "
                + "PREFIX text: <http://jena.apache.org/text#> "
                + "CONSTRUCT {?s rdfs:label ?l} "
                + "WHERE { ?s rdfs:label ?l.";
//        if (type!=null)
//                query+= "?s a <"+type.getURI()+"> .";
//        if (instanciatedProperty!=null) {
//                query+= "[ <"+instanciatedProperty.getURI()+"> ?s";
//                if (ownerId != -1) {
//                    query+="; <"+SempicOnto.ownerId+"> "+ownerId;
//                }
//                query+="] .";
//        }
//        if (labelContent!=null && !"".equals(labelContent)) {
//            query+= "FILTER regex(?l, \""+labelContent.toLowerCase()+"\", \"i\")";//"FILTER (STRSTARTS(LCASE(?l),\""+labelContent.toLowerCase()+"\")) .";
//            query+= "(?s) text:query (rdfs:label \""+labelContent.toLowerCase()+"*\") .";
//            
//        }
//        
//        query+= "FILTER (ISIRI(?s)) .";
//        query+="}";
//        //System.out.println(query);
        Model m = cnx.queryConstruct(query);
        List<Resource> res =  m.listSubjects().toList();
        return res;
    }
    
     public List<Resource> lookupInstances(Resource type, String prefix) {
         return lookupInstances(type,prefix,null,-1);
     }
    
    public List<Resource> createAnonInstances(Resource type, String prefix, Resource instanciatedProperty, long ownerId) {
        StringBuilder query = new StringBuilder(
                "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> "
                + "PREFIX text: <http://jena.apache.org/text#> "
                + "PREFIX owl: <http://www.w3.org/2002/07/owl#>"
                + "CONSTRUCT {_:i rdfs:label ?l ."
                + "_:i a ?c"
                + "} "
                + "WHERE { "
                + "?c a owl:Class . "
                +"{"
                + "?c rdfs:label ?cl . "
                + "VALUES ?det { \"a\"@en \"un(e)\"@fr \"unas/unos\"@es} ."
                + "FILTER (lang(?det)=lang(?cl)) ."
                + "BIND (CONCAT(?det,\"  \",?cl) AS ?l) ."
                + "} UNION { "
                + "FILTER (lang(?cl)=\"\") ."
                + "BIND (CONCAT(\"A  \",?cl) AS ?l) ."
                + "}");
//        if (instanciatedProperty!=null) {
//                query.append("FILTER (EXISTS {[ <").append(instanciatedProperty.getURI()).append("> ?s");
//                
//                if (ownerId != -1) {
//                    query.append("; <"+SempicOnto.ownerId+"> "+ownerId);
//                }
//                query.append("] . ?s a ?c}) .");
//                
//        }
        if (type!=null)
                query.append( "?c rdfs:subClassOf <"+type.getURI()+"> .");
        if (prefix!=null && !"".equals(prefix)) {
            query.append("FILTER regex(?l, \""+prefix.toLowerCase()+"\", \"i\")");
        }
        
        query.append("}");
        //System.out.println(query);
        Model m = cnx.queryConstruct(query.toString());
        return m.listSubjects().toList();
    }
    
    
    public List<Resource> getInstanciatedPictureTypes(long ownerId) {
        String query = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> "
                + "CONSTRUCT {?s rdfs:label ?l} "
                + "WHERE {?s rdfs:label ?l . "
                + "?s rdfs:subClassOf <"+SempicOnto.Photo+"> ."
                + "[ a ?s ";
//        if (ownerId!=-1) {
//            query+="; <"+SempicOnto.ownerId+"> "+ownerId;
//        }
        query+="] }";
        //System.out.println(query);
        Model m = cnx.queryConstruct(query);
        return m.listSubjects().toList();
    }
    
    
    public List<Resource> getInstancesAndTypes(String prefix, Resource type, Resource instanciatedProperty,long ownerId) {
        List<Resource> res = this.lookupInstances(type, prefix, instanciatedProperty,ownerId);
        res.addAll(this.createAnonInstances(type, prefix, instanciatedProperty,ownerId));
        return res;
    }
    
    public List<Resource> getInstancesAndTypes(String prefix, Resource type) {
        return getInstancesAndTypes(prefix,type,null,-1);
    }
   
    /*
    return all hasValue property restrictions that can be 
    built from objects of depicts property
    */
    public List<Resource> getDepictedPropertyRestrictions(String prefix, long ownerId) {
        String query = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
                        + "PREFIX text: <http://jena.apache.org/text#> " 
                        + "PREFIX owl: <http://www.w3.org/2002/07/owl#>"
                        + "CONSTRUCT { " +
                            "  _:x a owl:Restriction . " +
                            "  _:x owl:onProperty ?p ." +
                            "  _:x owl:hasValue ?v ." +
                            "  _:x rdfs:label ?l" +
                            " } WHERE {" +
                            "  [ <"+SempicOnto.represente+"> ?smt";
//        if (ownerId!=-1) {
//            query+="; <"+SempicOnto.ownerId+"> "+ownerId;
//        }
        query+="].";
               query+=      "  ?smt ?p ?v." +
                            "  ?p rdfs:label ?pl." +
                            "  ?v rdfs:label ?vl." +
                            "  BIND (CONCAT(?pl,\" \",?vl) AS ?l)."+
                            "  FILTER (LANG(?pl)=LANG(?vl))." ;//+
                            //"  FILTER (STRSTARTS(STR(?p),\""+SempicNamespaces.APP_ONTO+"\")) ." +
                            //"  FILTER (?p != <"+SempicNamespaces.APP_ONTO+"isDepictedIn>)";
                            if (prefix !=null && !"".equals(prefix)) {
                                query+= "FILTER (regex(LCASE(?l),\""+prefix.toLowerCase()+"\",\"i\"))";
                                //query+= "?s text:query (rdfs:label \""+prefix.toLowerCase()+"*\") .";
                            }
                            query+="}";
        //System.out.println(query);
        Model m = cnx.queryConstruct(query);
        return m.listSubjects().toList();
    } 
}
