/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.uga.miashs.sempic.rdf;

/**
 *
 * @author Jerome David <jerome.david@univ-grenoble-alpes.fr>
 */
import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdfconnection.*;

public class TestsRDF {
    
    private final static String ENDPOINT= "http://localhost:3030/sempic/";
    public final static String ENDPOINT_QUERY = ENDPOINT+"sparql"; // SPARQL endpoint
    public final static String ENDPOINT_UPDATE = ENDPOINT+"update"; // SPARQL UPDATE endpoint
    public final static String ENDPOINT_GSP = ENDPOINT+"data"; // Graph Store Protocol
    
    public static void main(String[] args) throws ParseException, IOException {
        
        RDFConnection cnx = RDFConnectionFactory.connect(ENDPOINT_QUERY, ENDPOINT_UPDATE, ENDPOINT_GSP);
        
        // Test exécution d'une requête //
//        QueryExecution qe = cnx.query("SELECT DISTINCT ?s WHERE {?s ?p ?o}");
//        ResultSet rs = qe.execSelect();
//        while (rs.hasNext()) {
//            QuerySolution qs = rs.next();
//            Resource r = qs.getResource("0");
//            System.out.println(qs.getResource("s"));
//        }
//        cnx.close();  
        //////////////////////////////////
        
        // Tests créations de ressources //
        PhotoStore s = new PhotoStore();
        AlbumStore a = new AlbumStore();
        UserStore u = new UserStore();
        
//        // ****** Test create user ******* //
        Resource usr = u.createUser((long)10, "Jean", "Michel"); // ==> fonctionne    
        Resource u1 = u.createUser((long)1, "bob", "Michel"); // ==> fonctionne    
        Resource u2 = u.createUser((long)2, "JL", "Michel"); // ==> fonctionne    

//        //******************************//
//        
//        // ****** Test create album ******* //
        Resource alb = a.createAlbum((long)22,(long)11, "Un beau titre", "Une description"); // ==> fonctionne    
//        //******************************//
//        
//        // ****** Test create photo ******* //
        Resource p1 = s.createPhoto((long)1,(long)22,(long)10, "Titre", "Description"); // ==> fonctionne    
        Resource p2 = s.createPhoto((long)2,(long)22,(long)10, "Titre", "Description"); // ==> fonctionne  
        Resource p3 = s.createPhoto((long)3,(long)22,(long)10, "Titre", "Description"); // ==> fonctionne  
        Resource p4 = s.createPhoto((long)4,(long)22,(long)10, "Titre", "Description"); // ==> fonctionne
        Resource p6 = s.createPhoto((long)6,(long)22,(long)1, "Titre", "Description"); // ==> fonctionne
        Resource p7 = s.createPhoto((long)7,(long)22,(long)2, "Titre", "Description"); // ==> fonctionne
        Resource p8 = s.createPhoto((long)8,(long)22,(long)1, "Titre", "Description"); // ==> fonctionne  
//        //******************************//
//               
//        /*** Tests ajouts annotations ***/       
        s.addAnnotationWhere(1, "Grenoble");
        s.addAnnotationWhere(2, "Lyon");
        s.addAnnotationWhere(3, "Tour de pise");
        s.addAnnotationWhere(4, "Paris");
//
        //s.addAnnotationWhen(33, new Date());
//        
//        s.addAnnotationWhat(33, "Chat");       
//        
//        s.addAnnotationPerson(33, 10);      
        /********************************/
        
//        s.deletePhoto(33);
        //a.deleteAlbum(22);
        //u.deleteUser(10);
        
//        boolean b = s.checkDBPediaResource("Paris");
//        System.out.println("EXISTS = "+b);
        
        // Test lecture photo //
        //Resource res = s.readPhoto(33);   // ==> fonctionne     
        //System.out.println(res.toString());
        /////////////////////////
        
        // Test recherche photos //
        List<Resource> rss;
        //rss = s.searchPhotos(null, null, null, null, null, -1); // Test pour toute les photos
        
        //rss = s.searchPhotos(null, null, null, 10); // test avec ownerId
        
        
        //rss = s.searchPictures("", (long) -1, "tour eiffel",  null, 10); // test avec where et ownerId
        rss = s.prepareSearch("", (long)-1, "France", null, "country", -1); // test avec ownerId
        rss.forEach(r -> System.out.println(r.getURI()));        
        ///////////////////////////
        
        // Test lecture album //
//        Resource alb = a.readAlbum(1);   // ==> fonctionne 
//        System.out.println(alb.toString());
//        System.out.println("Album URI = " + alb.getURI());
        /////////////////////////
        
        //s.deleteModel(m);
        //s.cnx.load(m);
        /*List<Resource> classes = s.listSubClassesOf(SempicOnto.Depiction);
        classes.forEach(c -> {System.out.println(c);});

        List<Resource> instances = s.createAnonInstances(classes);
        instances.forEach(i -> {
            System.out.println(i.getProperty(RDFS.label));
        });*/

        //s.deleteModel(m);
        //s.readPhoto(1).getModel().write(System.out,"turtle");
    }
}
