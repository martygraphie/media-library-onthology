/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.uga.miashs.sempic.rdf;

/**
 *
 * @author Jerome David <jerome.david@univ-grenoble-alpes.fr>
 */
public class Namespaces {
    
    public final static String photoNS = "http://miashs.univ-grenoble-alpes.fr/photo/";
    public final static String albumNS = "http://miashs.univ-grenoble-alpes.fr/album/";
    public final static String themeNS = "http://miashs.univ-grenoble-alpes.fr/theme/";
    public final static String representeNS = "http://miashs.univ-grenoble-alpes.fr/depiction/";
    //public final static String photoNS = "http://miashs.univ-grenoble-alpes.fr/photo";

    
    public static String getPhotoUri(long photoId) {
        return photoNS + photoId;
    }
    
    public static String getAlbumUri(long albumId) {
        return albumNS + albumId;
    }
    
    public static String getThemeUri(String themeName) {
        return themeNS + themeName;
    }
    
    public static String getUserUri(long userId){
        return representeNS + "person/user/" + userId;
    }
 
    public static String getPlaceUri(String name){
        return representeNS + "place/" + name;
    }
        
    public static String getCityUri(String name){
        return representeNS + "place/city/" + name;
    }
    
    public static String getCountryUri(String name){
        return representeNS + "place/country/" + name;
    }
    
    public static String getPersonUri(String name){
        return representeNS + "person/" + name;
    }
    
    public static String getAnimalUri(String name){
        return representeNS + "animal/" + name;
    }
    
    public static String getMonumentUri(String name){
        return representeNS + "monument/" + name;
    }
    
    public static String getWhatUri(String name){
        return representeNS + "tag/" + name;
    }
}
