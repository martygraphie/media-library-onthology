/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.uga.miashs.sempic.rdf;

import fr.uga.miashs.sempic.model.rdf.SempicOnto;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.shared.PrefixMapping;
import org.apache.jena.vocabulary.VCARD;

/**
 *
 * @author Pierre
 */
public class UserStore extends RDFStore{
    public final PrefixMapping prefixes;

    public UserStore() {
        prefixes = PrefixMapping.Factory.create();
        prefixes.withDefaultMappings(PrefixMapping.Standard);
        prefixes.setNsPrefix("sempic", SempicOnto.NS);
    }
    
    public Resource createUser(long userId, String firstname, String lastname) {
        // create an empty RDF graph and user entity
        Model m = ModelFactory.createDefaultModel();
        Resource user = m.createResource(Namespaces.getUserUri(userId), SempicOnto.User);
        
        //Add some property to the user instance
        user.addProperty(VCARD.FN, (firstname + " " + lastname));
        user.addProperty(VCARD.Given, firstname);
        user.addProperty(VCARD.Family, lastname);
        saveModel(m);
        return user;
    }
        
    public void deleteUser(long userId) {
        // get ressource and delete it
        Resource pRes = ResourceFactory.createResource(Namespaces.getUserUri(userId));
        deleteResource(pRes);
    }
}
