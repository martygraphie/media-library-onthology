/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.uga.miashs.sempic.rdf;

import fr.uga.miashs.sempic.model.rdf.SempicOnto;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.apache.jena.query.ParameterizedSparqlString;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.shared.PrefixMapping;
import org.apache.jena.vocabulary.DC;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

/**
 *
 * @author Pierre
 */
public class PhotoStore extends RDFStore {

    public final PrefixMapping prefixes;

    public PhotoStore() {
        prefixes = PrefixMapping.Factory.create();
        prefixes.withDefaultMappings(PrefixMapping.Standard);
        prefixes.setNsPrefix("sempic", SempicOnto.NS);
    }

    public Resource createPhoto(long photoId, long albumId, long ownerId, String title, String description) {
        // create an empty RDF graph
        Model m = ModelFactory.createDefaultModel();
        // create an instance of Photo in Model m
        Resource photo = m.createResource(Namespaces.getPhotoUri(photoId), SempicOnto.Photo);

        Resource alb = m.getResource(Namespaces.getAlbumUri(albumId));
        Resource user = m.getResource(Namespaces.getUserUri(ownerId));

        photo.addProperty(DC.description, description);
        photo.addProperty(DC.title, title);

        m.add(alb, SempicOnto.albumContain, photo);
        m.add(photo, SempicOnto.isInAlbum, alb);

        m.add(user, SempicOnto.ownsPic, photo);
        m.add(photo, SempicOnto.takenBy, user);

        saveModel(m);
        return photo;
    }

    public void deletePhoto(long photoId) {
        // create an instance of Photo in Model m
        Resource pRes = ResourceFactory.createResource(Namespaces.getPhotoUri(photoId));
        deleteResource(pRes);
    }

    /**
     * Add annotation to the picture i.e. triple <picture,p,o> is added to the
     * triple store. if o has properties, hey are also added.
     *
     * @param picture
     * @param p
     * @param o
     */
    public void addAnnotation(Resource picture, Property p, Resource o) {
        if (o == null) {
            return;
        }
        Model m = ModelFactory.createDefaultModel();
        if (o.getModel() != null) {
            m.add(o.listProperties());
        }
        m.add(picture, p, o);
        cnx.begin(ReadWrite.WRITE);
        cnx.load(m);
        cnx.commit();
        picture.getModel().add(m);
    }

    /**
     * @param photoId - Id of the photo
     * @param name - Name of the place
     */
    public void addAnnotationWhere(long photoId, String name) {
        Model m = ModelFactory.createDefaultModel();
        Resource p = m.getResource(Namespaces.getPhotoUri(photoId));
        List<String> placeNames = new ArrayList<>(Arrays.asList(name.split(" ")));
        String dbpediaName = "";
        for (String pn : placeNames) {
            // si dernier
            if (placeNames.indexOf(pn) == (placeNames.size() - 1)) {
                dbpediaName = dbpediaName + pn.substring(0, 1).toUpperCase() + pn.substring(1);
            } else {
                if (pn.equals("de")) {
                    dbpediaName = dbpediaName + pn.substring(0, 1).toLowerCase() + pn.substring(1) + "_";
                } else {
                    dbpediaName = dbpediaName + pn.substring(0, 1).toUpperCase() + pn.substring(1) + "_";
                }
            }
        }

        // Create resource place
        Resource place;
        if (checkIfPlaceExistsDbPedia(dbpediaName)) // si la place existe dans la dbpedia
        {
            place = m.createResource("http://dbpedia.org/resource/" + dbpediaName);
        } else // si la "place" n'existe pas dans la dbpedia
        {
            place = m.createResource(Namespaces.getPlaceUri(name.replace(" ", "").toLowerCase()));
        }
        place.addLiteral(RDFS.label, name);
        m.add(p, SempicOnto.takenWhere, place);
        saveModel(m);
    }

    public void addAnnotationWhen(long photoId, Date date) throws ParseException {
        Model m = ModelFactory.createDefaultModel();
        Resource p = m.getResource(Namespaces.getPhotoUri(photoId));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        String sdate = sdf.format(date);
        if (sdate != null && p != null) {
            p.addProperty(DC.date, sdate);
        }
        saveModel(m);
        transformWhenDateFormat();
        deleteNotCorrectWhenDate();
    }

    /**
     * Provide a valid userId and photoId
     *
     * @param photoId
     * @param userId - A valid userId or -1
     */
    public void addAnnotationPerson(long photoId, long userId) {
        Model m = ModelFactory.createDefaultModel();
        Resource p = m.getResource(Namespaces.getPhotoUri(photoId));
        Resource u = null;
        if (userId != -1) {
            u = m.getResource(Namespaces.getUserUri(userId));
        }
        if (u != null) {
            m.add(p, SempicOnto.represente, u);
        }
        saveModel(m);
    }

    /**
     * @param photoId
     * @param name
     */
    public void addAnnotationWhat(long photoId, String name) {
        Model m = ModelFactory.createDefaultModel();
        Resource p = m.getResource(Namespaces.getPhotoUri(photoId));
        Resource w = m.createResource(Namespaces.getWhatUri(name.replace(" ", "").toLowerCase()));
        m.add(p, SempicOnto.represente, w);
        saveModel(m);
    }

    public void addAnnotationAnimal(long photoId, String name) {
        Model m = ModelFactory.createDefaultModel();
        Resource p = m.getResource(Namespaces.getPhotoUri(photoId));

        if (!name.isEmpty()) {
            Resource a = m.getResource(Namespaces.getAnimalUri(name).replace(" ", "").toLowerCase());
            if (!m.containsResource(a)) {
                a = m.createResource(Namespaces.getAnimalUri(name).replace(" ", "").toLowerCase(), SempicOnto.Animal);
            }

            m.add(p, SempicOnto.represente, a);
        }
        saveModel(m);
    }

    public void addAnnotationMonument(long photoId, String name) {
        Model m = ModelFactory.createDefaultModel();
        Resource p = m.getResource(Namespaces.getPhotoUri(photoId));

        if (!name.isEmpty()) {
            Resource mo = m.getResource(Namespaces.getMonumentUri(name.replace(" ", "").toLowerCase()));
            if (!m.containsResource(mo)) {
                mo.addProperty(DC.title, name);
            }

            m.add(p, SempicOnto.represente, mo);
        }
        saveModel(m);
    }

    public void addAnnotationTheme(long photoId, String themeName) {
        Model m = ModelFactory.createDefaultModel();
        Resource p = m.getResource(Namespaces.getPhotoUri(photoId));

        if (!themeName.isEmpty()) {
            Resource t = m.getResource(Namespaces.getThemeUri(themeName).replace(" ", "").toLowerCase());

            if (!m.containsResource(t)) {
                t = m.createResource(Namespaces.getThemeUri(themeName).replace(" ", "").toLowerCase(), SempicOnto.Theme);
            }

            m.add(p, SempicOnto.isInTheme, t);
            m.add(t, SempicOnto.themeHas, p);
        }
        saveModel(m);
    }

    public void setAnnotation(Resource picture, Property annotProp, Resource r) {
        deleteAnnotation(picture, annotProp);
        addAnnotation(picture, annotProp, r);
    }

    /**
     * delete all the triple with subject picture and property p
     *
     * @param picture
     * @param p
     */
    public void deleteAnnotation(Resource picture, Property p) {
        cnx.update("DELETE  WHERE { <" + picture.getURI() + "> <" + p.getURI() + "> ?o } ");
        if (picture.getModel() != null) {
            picture.removeAll(p);
        }
    }

    /**
     * Delete the triple <picture,p,o> from the triple store and also the
     * resource o if it is a blank node
     *
     * @param picture
     * @param p
     * @param o
     */
    public void deleteAnnotation(Resource picture, Property p, Resource o) {
        cnx.begin(ReadWrite.WRITE);
        // can add a clause that check that p is a subproperty of depicts
        if (o.isAnon()) {
            cnx.update("DELETE WHERE { "
                    + "<" + picture.getURI() + "> <" + p.getURI() + "> ?o . "
                    + "?o <" + RDFS.label + "> \"" + o.getProperty(RDFS.label).getString() + "\" ."
                    + "?o ?p ?x}");
        } else {
            cnx.update("DELETE DATA { <" + picture.getURI() + "> <" + p.getURI() + "> <" + o.getURI() + "> } ");
        }
        cnx.commit();
        if (picture.getModel() != null) {
            picture.getModel().removeAll(picture, p, o);
        }
    }

    /**
     * Check if ressource exists in dbpedia based on rdfs:label
     *
     * @param name - Name of the resource starting with an UPPERCASE
     * @return
     */
    public boolean checkIfPlaceExistsDbPedia(String name) {
        String service = "http://dbpedia.org/sparql";
        String query = "PREFIX dbo: <http://dbpedia.org/resource/>"
                + "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
                + "SELECT ?val WHERE { dbo:" + name + " rdfs:label  ?val. }";
        QueryExecution qe = QueryExecutionFactory.sparqlService(service, query);
        ResultSet rs = qe.execSelect();
        return rs.hasNext();
    }

    /**
     * Add dateTime format to date when
     */
    public void transformWhenDateFormat() {
        ParameterizedSparqlString pss = new ParameterizedSparqlString(prefixes);
        pss.setBaseUri(Namespaces.photoNS);
        cnx.begin(ReadWrite.WRITE);
        cnx.update("PREFIX dc: <http://purl.org/dc/elements/1.1/>"
                + "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>"
                + "INSERT { ?s2 dc:date ?date_fixed . }"
                + "WHERE {"
                + "?s2 dc:date ?date ."
                + "BIND (STRDT(REPLACE(?date, \" \", \"T\"), xsd:dateTime) AS ?date_fixed)"
                + "}");
        cnx.commit();
    }

    /**
     * Delete date not at dateTime format
     */
    public void deleteNotCorrectWhenDate() {
        ParameterizedSparqlString pss = new ParameterizedSparqlString(prefixes);
        pss.setBaseUri(Namespaces.photoNS);
        cnx.update("PREFIX dc: <http://purl.org/dc/elements/1.1/>"
                + "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>"
                + "DELETE { ?s dc:date ?date . }"
                + "WHERE {"
                + "?s dc:date ?date ."
                + "FILTER( datatype(?date) != xsd:dateTime)"
                + "}");
        cnx.commit();
    }

    /**
     * Query a Photo and retrieve all the direct properties of the photo and if
     * the property are depic, takenIn or takenBy, it also retrieve the labels
     * of the object of these properties
     *
     * @param id
     * @return
     */
    public Resource readPhoto(long id) {
        String pUri = Namespaces.getPhotoUri(id);
        ParameterizedSparqlString pss = new ParameterizedSparqlString(prefixes);
        pss.setBaseUri(Namespaces.photoNS);

        pss.setCommandText(
                "CONSTRUCT {"
                + "?photo ?p ?o ."
                + "?photo ?p1 ?o1 ."
                + "?o1 rdfs:label ?o2 ."
                + "} WHERE { "
                + "?photo ?p ?o ."
                + "OPTIONAL {"
                + "?photo ?p1 ?o1 ."
                + "?o1 rdfs:label ?o2 ."
                + "FILTER (?p1 IN (<" + SempicOnto.represente + ">,<" + SempicOnto.takenWhere + ">,<" + SempicOnto.takenBy + ">)) "
                + "}"
                + "}");
        pss.setIri("photo", pUri);

        Model m = cnx.queryConstruct(pss.asQuery());
        String str = cnx.queryConstruct(pss.asQuery()).toString();
        return m.getResource(pUri);
    }

    /**
     * Method to be called from SearchController
     *
     * @param what -- List of WHAT Tags
     * @param who
     * @param where
     * @param when
     * @param advancedFilter
     * @param ownerId -- Valid userId or -1
     * @return
     */
    public List<Resource> prepareSearch(String what, long who, String where, Date when, String advancedFilter, long ownerId) {
        List<Resource> depicts = new ArrayList<>();
        Resource takenWhere = null;
        Model m = ModelFactory.createDefaultModel();

        if (!what.isEmpty()) {
            depicts.add(m.getResource(Namespaces.getWhatUri(what)));
        }

        if (who != -1) {
            depicts.add(m.getResource(Namespaces.getUserUri(who)));
        }

        if (!where.isEmpty() && !advancedFilter.equals("noplaces")) {
            List<String> placeNames = new ArrayList<>(Arrays.asList(where.split(" ")));
            String dbpediaName = "";
            for (String pn : placeNames) {
                // si dernier
                if (placeNames.indexOf(pn) == (placeNames.size() - 1)) {
                    dbpediaName = dbpediaName + pn.substring(0, 1).toUpperCase() + pn.substring(1);
                } else {
                    dbpediaName = dbpediaName + pn.substring(0, 1).toUpperCase() + pn.substring(1) + "_";
                }
            }
            if (checkIfPlaceExistsDbPedia(dbpediaName)) { // si la place existe dans la dbpedia 
                takenWhere = m.getResource("http://dbpedia.org/resource/" + dbpediaName);
            } else { // si la "place" n'existe pas dans la dbpedia
                takenWhere = m.getResource(Namespaces.getPlaceUri(where.replace(" ", "").toLowerCase()));
            }
        }
        return searchPhotos(depicts, when, takenWhere, advancedFilter, ownerId);
    }

    /*
    Can be simplified
     */
    public List<Resource> searchPhotos(List<Resource> depicts, Date takenWhen, Resource takenWhere, String advancedFilter, long ownerId) {
        StringBuilder query = new StringBuilder();
        query.append("PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> "
                + "PREFIX dbo: <http://dbpedia.org/ontology/> "
                + "CONSTRUCT {?p a <" + SempicOnto.Photo + ">} "
                + "WHERE {"
                + "?p a <" + SempicOnto.Photo + "> .");

        if (takenWhere != null) {
            if (advancedFilter.equals("country")) {
                query.append("?p <" + SempicOnto.takenWhere + "> ?place .");
            } else {
                query.append("?p <" + SempicOnto.takenWhere + "> <" + takenWhere + "> .");
            }
        }

        for (Resource t : depicts) {
            if (t.isAnon()) {
                query.append("?p <" + SempicOnto.represente + "> ?d. ?d a <" + t.getPropertyResourceValue(RDF.type) + "> .");
            } else {
                query.append("?p <" + SempicOnto.represente + "> <" + t + "> .");
            }
        };

        if (ownerId != -1) {
            query.append("?p <" + SempicOnto.takenBy + "> <" + Namespaces.getUserUri(ownerId) + "> .");
        }
        if (takenWhen != null && (!advancedFilter.equals("year") && !advancedFilter.equals("month"))) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
            String date = sdf.format(takenWhen);
            date = date.replace(" ", "T");
            query = replaceLast(query.toString(), ".", "");
            query.append("; <").append(DC.date).append("> ?date  FILTER(?date = \"").append(date).append("\"^^xsd:dateTime)");
        }
        if (!advancedFilter.isEmpty()) {
            query.append(getAdvancedFilter(advancedFilter, takenWhen, takenWhere));
        }
        query.append("}");
        System.out.println(query);
        return cnx.queryConstruct(query.toString()).listSubjects().toList();
    }

    private StringBuilder replaceLast(String string, String substring, String replacement) {
        StringBuilder sb = new StringBuilder();
        int index = string.lastIndexOf(substring);
        return sb.append(string.substring(0, index) + replacement + string.substring(index + substring.length()));
    }

    /*
      Function to return advanced filter.
     */
    private String getAdvancedFilter(String filter, Date takenWhen, Resource takenWhere) {
        String action = null;
        switch (filter) {
            case "noplaces":
                action = "FILTER NOT EXISTS { ?p <" + SempicOnto.takenWhere + "> ?place . } .";
                break;

            case "country":
                action = "BIND(IRI(?place) AS ?cityResource) SERVICE <http://dbpedia.org/sparql> { ?cityResource dbo:country <" + takenWhere + "> . }";
                break;

            case "year":
            case "month":
                SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
                String year = yearFormat.format(takenWhen);
                if (filter.equals("year")) {
                    action = "?p <" + DC.date + "> ?d.  FILTER(YEAR(?d) = " + year + " ) .";
                } else {
                    SimpleDateFormat monthFormat = new SimpleDateFormat("MM");
                    String month = monthFormat.format(takenWhen);
                    action = "?p <" + DC.date + "> ?d.  FILTER(MONTH(?d) = " + month + " && YEAR(?d) = " + year + " ) .";
                }
                break;
            case "usercount":
                action = "?p <" + SempicOnto.takenBy + "> ?selectUser. BIND(?selectUser AS ?user) { SELECT ?user WHERE { ?user a <" + SempicOnto.User + "> . ?user <" + SempicOnto.ownsPic + "> ?pic .}GROUP BY ?user  HAVING (COUNT(?pic) >= 2)}";
        }
        return action;
    }
}
