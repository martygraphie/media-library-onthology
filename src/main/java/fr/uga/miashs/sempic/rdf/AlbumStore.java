/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.uga.miashs.sempic.rdf;

import fr.uga.miashs.sempic.model.rdf.SempicOnto;
import org.apache.jena.query.ParameterizedSparqlString;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.shared.PrefixMapping;
import org.apache.jena.vocabulary.DC;

/**
 *
 * @author Pierre
 */
public class AlbumStore extends RDFStore{
    public final PrefixMapping prefixes;

    public AlbumStore() {
        prefixes = PrefixMapping.Factory.create();
        prefixes.withDefaultMappings(PrefixMapping.Standard);
        prefixes.setNsPrefix("sempic", SempicOnto.NS);
    }
    
    public Resource createAlbum(long albumId, long ownerId, String title, String description) {
        // create an empty RDF graph
        Model m = ModelFactory.createDefaultModel();
        
        // create an instance of album  in Model m
        Resource alb = m.createResource(Namespaces.getAlbumUri(albumId), SempicOnto.Album);
        
        // Add annotations
        alb.addProperty(DC.title, title);
        alb.addProperty(DC.description, description);
        Resource usr = m.getResource(Namespaces.getUserUri(ownerId));

        m.add(usr, SempicOnto.ownsAlb, alb);
        
//        m.write(System.out, "turtle");
        //Save
        saveModel(m);
        return alb;
    }
        
    public void deleteAlbum(long albumId) {
        // get ressource and delete it
        Resource pRes = ResourceFactory.createResource(Namespaces.getAlbumUri(albumId));
        deleteResource(pRes);
    }
    
        /**
     * Query a Photo and retrieve all the direct properties of the photo and if
     * the property are depic, takenIn or takenBy, it also retrieve the labels
     * of the object of these properties
     *
     * @param id
     * @return
     */
    public Resource readAlbum(long id) {
        String pUri = Namespaces.getAlbumUri(id);
        ParameterizedSparqlString pss = new ParameterizedSparqlString(prefixes);
        pss.setBaseUri(Namespaces.albumNS);
        
        pss.setCommandText(
                "CONSTRUCT {"
                        + "?album ?p ?o ."
                        + "?album ?p1 ?o1 ."
                        + "?o1 rdfs:label ?o2 ."
                + "} WHERE { "
                        + "?album ?p ?o ."
                        + "OPTIONAL {"
                        + "?album ?p1 ?o1 ."
                        + "?o1 rdfs:label ?o2 ."
                        + "FILTER (?p1 IN (<" + SempicOnto.ownsAlb + ">)) "
                        +"}"
                 + "}");
        pss.setIri("photo", pUri);
        
        Model m = cnx.queryConstruct(pss.asQuery());
        String str = cnx.queryConstruct(pss.asQuery()).toString();
        return m.getResource(pUri);
    }
}
