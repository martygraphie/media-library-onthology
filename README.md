## ABSTRACT

The goal of the project is to create an application allowing, from an ontology, to create RDF resources. 
The project uses the JENA technology. 

## BACKGROUND

The application does not propose an authentication system. 
Several methods are available to create resources and add RDF triples to the triple store.
A test file is available and can be modified in order to test the functioning of the methods. 

## PREREQUISITES

Download and install the following servers: 
  * Apache Jena Fuseki : 3.13.0

## INSTALLATION
    1. Download the SempicRDF project and unzip it to the same level as the Apache Jena Fuseki server.
    2. Open the SempicRDF project and build it. 
    3. In order to test the SempicRDF project, it is necessary to launch the Apache Jena Fuseki server (fuseki-server.bat 
       for Windows or fuseki-server.jar for Linux/Mac). 

## IMPLEMENTATION

An ontology was defined using the Protégé software (https://protege.stanford.edu/). 

We have used the website http://visualdataweb.de/webvowl to create a graphical representation of the ontology. 

![OWL Diagram](/src/main/resources/ontologyschema.jpg?raw=true "OWL Diagram")

Different classes have been created:

#### UserStore:
* A method to add a user (its URI) to the triple Store
* A method allows to remove the user (his Uri) and all his occurrences in any triplet of the triple Store

#### AlbumStore:
* A method allows to add an album (its URI) to the triple Store
* A method allows to delete the album (its Uri) and all its occurrences in any triplet of the triple Store

#### PhotoStore:
* One method allows to add a photo (its URI) to the triple Store
* Several methods allow to add annotations (RDF triplets) to the triple Store (who/what/when annotations). 
* A method allows to delete the photo (its Uri) and all its occurrences in any triplet of the triple Store
* A method allows you to perform different searches:
  - Search on the annotations who/what/when/where
  - Search for photos that have no locations 
  - Search for photos by year/month
  - Search for users with more than one photo

A **TestsRDF** file can be run/modified and allows to test the different methods. 

You can view your rdf resources here: http://localhost:3030/sempic-data/data   

